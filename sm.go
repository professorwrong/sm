package sm

type State func(args ...interface{})

type statemachine struct {
	curSt State
	nxtSt State
}

func (self *statemachine) PreRun() {

}

func (self *statemachine) PostRun() {

}

func (self *statemachine) Run() {
	self.PreRun()
	defer self.PostRun()

	for {
		if self.nxtSt != nil {
			self.curSt = self.nxtSt
			self.nxtSt = nil
		} else if self.curSt == nil {
			return
		}

		self.curSt()
		self.curSt = nil

		if self.nxtSt == nil {
			return
		}
	}
}

func (self *statemachine) SetNextState(aSt State) bool {
	self.nxtSt = aSt
	return true
}
